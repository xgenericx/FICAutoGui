#InstallKeybdHook
#Persistent
toggle = 0
#MaxThreadsPerHotkey 2

FICAutoGui()
!s::
WinActivate , FI Simulator
SendMessage, 0x1330, 6,, SysTabControl321, FI Simulator
sleep , 100
ControlGet , Curr , Checked , , Button86 , FI Simulator
Stat = Curr
Loop , 10 {
	ControlGet , Stat , Checked , , Button86 , FI Simulator
	if (Stat <> Curr) {
		break
	}
	if (Stat == Curr) {
		ControlClick , Button86 , FI Simulator
	}
	sleep , 100
}
return

!m::
Toggle  := !Toggle
While Toggle{
	Click
	sleep , 1000
}
return

!e::
Exitapp

#p::Pause
Pause , Toggle, OperateOnUnderlyingThread

EnableWaferDropDetection(EnableWaferDropDetection) {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	WinActivate , ahk_class SunAwtFrame
	ClickUIButton("Misc",,,720,150)
	ClickUIButton("FISetup",,,300)
	ClickUIButton("EnableWaferDropDetection",,,,,350)
	ClickUIButton(EnableWaferDropDetection)
}

BatchSet() {
	global
	BatchCommand%BatchIndex%:=Command
	if (Command == "Appr") OR (Command == "Mpre") OR (Command == "Trans") OR (Command == "GTSShipping") OR (Command == "MPREShippingMPRE") OR (Command == "Tmo"){
		BatchBlade%BatchIndex%:=Blade
	} else {
		BatchBlade%BatchIndex%:=empty
	}
	if (Command == "Appr") OR (Command == "Mpre") OR (Command == "Trans") OR (Command == "GTSShipping") OR (Command == "MPREShippingMPRE") OR (Command == "Maps") OR (Command == "Tmo"){
		BatchSpeed%BatchIndex%:=Speed
	} else {
		BatchSpeed%BatchIndex%:=empty
	}
	if (Command == "Trans") {
		BatchAlignToo%BatchIndex%:=AlignToo
	} else {
		BatchAlignToo%BatchIndex%:=empty
	}
	if (Command == "Tmo") {
		BatchStepping%BatchIndex%:=Stepping
	} else {
		BatchStepping%BatchIndex%:=empty
	}
	if (Command == "EnableWaferDropDetection") {
		BatchEnableWaferDropDetection%BatchIndex%:=EnableWaferDropDetection
	} else {
		BatchEnableWaferDropDetection%BatchIndex%:=empty
	}
	Gui , ListView , ListBatch
	i = 1
	Loop {
		if (R1port%i%==empty) {
			if (Command == "ModePhtm") OR (Command == "ModeTerm") {
				BatchR1port%BatchIndex%_%i% := empty
				BatchR1slots%BatchIndex%_%i%:= empty
			}
			break
		} else {
			R1pSlots%i% := ", " R1port%i% ":" R1slots%i%
			BatchR1port%BatchIndex%_%i% := R1port%i%
			BatchR1slots%BatchIndex%_%i%:=R1slots%i%
		}
		i += 1
	}
	i = 1
	Loop {
		if (R2port%i%==empty) {
			if (Command == "ModePhtm") OR (Command == "ModeTerm") {
				BatchR2port%BatchIndex%_%i% := empty
				BatchR2slots%BatchIndex%_%i%:= empty
			}
			break
		} else {
			R2pSlots%i% := ", " R2port%i% ":" R2slots%i%
			BatchR2port%BatchIndex%_%i% := R2port%i%
			BatchR2slots%BatchIndex%_%i%:=R2slots%i%
		}
		i += 1
	}
	R1PS := R1port1 ":" R1slots1 R1pSlots2 R1pSlots3 R1pSlots4 R1pSlots5 R1pSlots6 R1pSlots7 R1pSlots8 R1pSlots9 R1pSlots10 R1pSlots11
	R2PS := R2port1 ":" R1slots1 R2pSlots2 R2pSlots3 R2pSlots4 R2pSlots5 R2pSlots6 R2pSlots7 R2pSlots8 R2pSlots9 R2pSlots10 R2pSlots11
	if (R1port1<>empty) {
		LV_Add("",BatchIndex,Command,"R1: " R1PS ,BatchBlade%BatchIndex%,BatchSpeed%BatchIndex%,BatchAlignToo%BatchIndex%,BatchStepping%BatchIndex%,BatchEnableWaferDropDetection%BatchIndex%)
	}
	if (R2port1<>empty) {	
		LV_Add("",BatchIndex,Command,"R2: " R2PS ,BatchBlade%BatchIndex%,BatchSpeed%BatchIndex%,BatchAlignToo%BatchIndex%,BatchStepping%BatchIndex%,BatchEnableWaferDropDetection%BatchIndex%)
	}
	BatchIndex += 1
}

BatchGo() {
	global
	index := 1
	BatchRun := 1
	Loop {
		Blade:=BatchBlade%index%
		Speed:=BatchSpeed%index%
		AlignToo:=BatchAlignToo%index%
		Stepping:=BatchStepping%index%
		EnableWaferDropDetection:=BatchEnableWaferDropDetection%index%
		i = 1
		Loop {
			if (BatchR1port%index%_%i% == empty) {
				break
			}
			R1port%i%:=BatchR1port%index%_%i%
			R1slots%i%:=BatchR1slots%index%_%i%
			i += 1
		}
		i = 1
		Loop {
			if (BatchR2port%index%_%i% == empty) {
				break
			}
			R2port%i%:=BatchR2port%index%_%i%
			R2slots%i%:=BatchR2slots%index%_%i%
			i += 1
		}
		if (BatchCommand%index%=="Trans") {
			Trans()
		} else if (BatchCommand%index%=="Mpre") {
			Mpre()
		} else if (BatchCommand%index%=="Maps") {
			Maps()
		} else if (BatchCommand%index%=="Appr") {
			Appr()
		} else if (BatchCommand%index%=="GTSShipping") {
			GTSShipping()
		} else if (BatchCommand%index%=="MPREShippingMPRE") {
			MPREShippingMPRE()
		} else if (BatchCommand%index%=="ModePhtm") {
			ModePhtm()
		} else if (BatchCommand%index%=="ModeTerm") {
			ModeTerm()
		} else if (BatchCommand%index%=="Tmo") {
			Tmo()
		} else if (BatchCommand%index%=="EnableWaferDropDetection") {
			EnableWaferDropDetection(EnableWaferDropDetection)
		}
		index += 1
		if (BatchCommand%index% == empty) {
			BatchRun := 0
			return
		}
	}
	BatchRun := 0
}

Setspd(Robot,RobotSpeed) {
	WinActivate , ahk_class SunAwtFrame
	if (Robot="Robot1") {
		Imagesearch , fx , fy , 0 , 0 , 1043 , 200 , %A_ScriptDir%\Resources\FIROBOT1MAIN.PNG
		if (errorlevel == 0) {
			ClickUIButton("Ready")
			goto SetSpdJump
		}
	}
	sleep , 200
	ClickUIButton("Misc",,,720,150)
	sleep , 200
	ClickUIButton(Robot,,,550,300)
SetSpdJump:
	sleep , 3000
	WaitReady(300000)
	ClickUIButton("Service")
	ClickUIButton("setdesiredrobotspeed",,,,,100,30)
	ClickUIButton(RobotSpeed,600,580)
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
}

FICAutoGui() {
	ChoosePortsGui()
}

ImageWait(ByRef p_x, ByRef p_y, p_x1, p_y1, p_x2, p_y2, p_img, p_waitms=0, p_checkinterval="") {
	global waitrob
	if (p_checkinterval="")
		p_checkinterval=519
	ts:=A_TickCount
	Loop {
		ImageSearch, p_x, p_y, p_x1, p_y1, p_x2, p_y2, %p_img%
		if (errorlevel=0 || p_waitms && A_TickCount-ts>=p_waitms)
			break
		Imagesearch , fx , fy , 0 , 0 , 1043 , 790 , %A_ScriptDir%\Resources\transactiontimeout.PNG
		if (errorlevel==0) {
			ClickUIButton("Close")
			sleep , 1000
			if (waitrob=="R1") {
				StatR1DoneWait(0,0,400,790)
				ClickUIButton("Misc")
				ClickUIButton("Robot1")
			} else if (waitrob=="R2") {
				StatR2DoneWait(0,0,400,790)
				ClickUIButton("Misc")
				ClickUIButton("Robot2")
			}
		}
		Imagesearch , fx , fy , 0 , 0 , 1043 , 790 , %A_ScriptDir%\Resources\done.PNG
		if (errorlevel==0) {
			break
		}
		if (p_waitms && (A_TickCount-ts)+p_checkinterval>=p_waitms)
			p_checkinterval:=(p_waitms-(A_TickCount-ts))/2
		if (p_checkinterval>19)
			Sleep, %p_checkinterval%
	}
}

StatR1DoneWait(p_x1, p_y1, p_x2, p_y2, p_waitms=0, p_checkinterval="") {
	global FoundItem
	if (p_checkinterval="")
		p_checkinterval=519
	ts:=A_TickCount
	ClickUIButton("Misc")
	ClickUIButton("Robot1")
	ClickUIButton("Terminal")
	ClickUIButton("NewCommand",,,,,400)
	Loop , 4 {
		Send {Raw}<1:stat,r1>??
		Send {enter}
		sleep , 2000
		FindUIButton("statR1Done",,,,,,,0,0)
		if (FoundItem == 1 || p_waitms && A_TickCount-ts>=p_waitms) {
			return
		}
	}
	Loop {
		FindUIButton("smallRes0",,,,,,,0,0)
		if (FoundItem == 1 || p_waitms && A_TickCount-ts>=p_waitms) {
			break
		}
		if (p_waitms && (A_TickCount-ts)+p_checkinterval>=p_waitms)
			p_checkinterval:=(p_waitms-(A_TickCount-ts))/2
		if (p_checkinterval>19)
			Sleep, %p_checkinterval%
	}
}

StatR2DoneWait(p_x1, p_y1, p_x2, p_y2, p_waitms=0, p_checkinterval="") {
	global FoundItem
	if (p_checkinterval="")
		p_checkinterval=519
	ts:=A_TickCount
	ClickUIButton("Misc")
	ClickUIButton("Robot2")
	ClickUIButton("Terminal")
	ClickUIButton("NewCommand",,,,,400)
	Loop , 4 {
		Send {Raw}<1:stat,r2>??
		Send {enter}
		sleep , 2000
		FindUIButton("statR2Done",,,,,,,0,0)
		if (FoundItem == 1 || p_waitms && A_TickCount-ts>=p_waitms) {
			return
		}
	}
	Loop {
		FindUIButton("smallRes0",,,,,,,0,0)
		if (FoundItem == 1 || p_waitms && A_TickCount-ts>=p_waitms) {
			break
		}
		if (p_waitms && (A_TickCount-ts)+p_checkinterval>=p_waitms)
			p_checkinterval:=(p_waitms-(A_TickCount-ts))/2
		if (p_checkinterval>19)
			Sleep, %p_checkinterval%
	}
}

WaitReady(Timeout) {
	
	sleep , 200
	img = *10 %A_ScriptDir%\Resources\ready.png
	ImageWait(Foundx , Foundy , 70 , 150 , 250 , 250 , img, Timeout )
	Imagesearch , fx , fy , 0 , 0 , 1043 , 790 , %A_ScriptDir%\Resources\transactiontimeout.PNG
	if (errorlevel==0) {
		ClickUIButton("Close")
	}
}

WaitRes0(Timeout) {
	sleep , 200
	img = *10 %A_ScriptDir%\Resources\Res0.png
	ImageWait(Foundx , Foundy , 0 , 0 , 1043 , 790 , img, Timeout )
}

trans() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	if (R1Port1<>empty) {
		setspd("Robot1",Speed)
	}
	i = 1
	j = 1
	While (R1port%i%<>empty) {
		waitrob := "R1"
		j := i
		While (R1port%j%<>empty) {
			s := R1port%i%
			ss := R1slots%i%
			d := R1port%j%
			ds := R1slots%j%
			if (s == d) {
				j += 1
				goto , continuetransR1
			}
			sleep , 2000
			FindUIButton("TransferCommand",,,,,,,0)
			if (FoundItem==1) {
				FindUIButton("ApplyGray",,,,,,,0)
				if (FoundItem==1) {
					Transfer2("Robot1",s,ss,d,ds,AlignToo,Blade)
					WaitReady(300000)
					FindUIButton("Apply",,,,,,,0)
					if (FoundItem==1) {
						ClickUIButton("Apply")
					} else if (FoundItem==0) {
						Transfer("Robot1",s,ss,d,ds,AlignToo,Blade)
					}
				}
			} else if (FoundItem==0) {
				WaitReady(300000)
				Transfer("Robot1",s,ss,d,ds,AlignToo,Blade)
			}
			temp := s
			s := d
			d := temp
			stemp := ss
			ss := ds
			ds := stemp
			sleep , 2000
			FindUIButton("TransferCommand",,,,,,,0)
			if (FoundItem==1) {
				FindUIButton("ApplyGray",,,,,,,0)
				if (FoundItem==1) {
					Transfer2("Robot1",s,ss,d,ds,AlignToo,Blade)
					WaitReady(300000)
					FindUIButton("Apply",,,,,,,0)
					if (FoundItem==1) {
						ClickUIButton("Apply")
					} else if (FoundItem==0) {
						Transfer("Robot1",s,ss,d,ds,AlignToo,Blade)
					}
				}
			} else if (FoundItem==0) {
				WaitReady(300000)
				Transfer("Robot1",s,ss,d,ds,AlignToo,Blade)
			}
			j += 1
			if (R1port%j%==empty) {
				temp := (i + 1)
				s := d
				d := R1port%temp%
				ss := ds
				ds := R1slots%temp%
				sleep , 2000
				FindUIButton("TransferCommand",,,,,,,0)
				if (FoundItem==1) {
					FindUIButton("ApplyGray",,,,,,,0)
					if (FoundItem==1) {
						Transfer2("Robot1",s,ss,d,ds,0,Blade)
						WaitReady(300000)
						FindUIButton("Apply",,,,,,,0)
						if (FoundItem==1) {
							ClickUIButton("Apply")
						} else if (FoundItem==0) {
							Transfer("Robot1",s,ss,d,ds,0,Blade)
						}
					}
				} else if (FoundItem==0) {
					WaitReady(300000)
					Transfer("Robot1",s,ss,d,ds,0,Blade)
				}
			}
continuetransR1:
			abra = 0 
		}
		i += 1
		if (R1port%i%==empty) {
			s := d
			d := R1port1
			ss := ds
			ds := R1slots1
			sleep , 2000
				FindUIButton("TransferCommand",,,,,,,0)
				if (FoundItem==1) {
					FindUIButton("ApplyGray",,,,,,,0)
					if (FoundItem==1) {
						Transfer2("Robot1",s,ss,d,ds,0,Blade)
						WaitReady(300000)
						FindUIButton("Apply",,,,,,,0)
						if (FoundItem==1) {
							ClickUIButton("Apply")
						} else if (FoundItem==0) {
							Transfer("Robot1",s,ss,d,ds,0,Blade)
						}
					}
				} else if (FoundItem==0) {
					WaitReady(300000)
					Transfer("Robot1",s,ss,d,ds,0,Blade)
				}
		}
	}
	if (R2Port1<>empty) {
		setspd("Robot2",Speed)
	}
	i = 1
	j = 1
	While (R2port%i%<>empty) {
		waitrob := "R2"
		j := i
		While (R2port%j%<>empty) {
			s := R2port%i%
			ss := R2slots%i%
			d := R2port%j%
			ds := R2slots%j%
			if (s == d) {
				j += 1
				goto , continuetransR2
			}
			sleep , 2000
				FindUIButton("TransferCommand",,,,,,,0)
				if (FoundItem==1) {
					FindUIButton("ApplyGray",,,,,,,0)
					if (FoundItem==1) {
						Transfer2("Robot2",s,ss,d,ds,AlignToo,Blade)
						WaitReady(300000)
						FindUIButton("Apply",,,,,,,0)
						if (FoundItem==1) {
							ClickUIButton("Apply")
						} else if (FoundItem==0) {
							Transfer("Robot2",s,ss,d,ds,AlignToo,Blade)
						}
					}
				} else if (FoundItem==0) {
					WaitReady(300000)
					Transfer("Robot2",s,ss,d,ds,AlignToo,Blade)
				}
			temp := s
			s := d
			d := temp
			stemp := ss
			ss := ds
			ds := stemp
			sleep , 2000
				FindUIButton("TransferCommand",,,,,,,0)
				if (FoundItem==1) {
					FindUIButton("ApplyGray",,,,,,,0)
					if (FoundItem==1) {
						Transfer2("Robot2",s,ss,d,ds,AlignToo,Blade)
						WaitReady(300000)
						FindUIButton("Apply",,,,,,,0)
						if (FoundItem==1) {
							ClickUIButton("Apply")
						} else if (FoundItem==0) {
							Transfer("Robot2",s,ss,d,ds,AlignToo,Blade)
						}
					}
				} else if (FoundItem==0) {
					WaitReady(300000)
					Transfer("Robot2",s,ss,d,ds,AlignToo,Blade)
				}
			j += 1
			if (R2port%j%==empty) {
				temp := (i + 1)
				s := d
				d := R2port%temp%
				ss := ds
				ds := R2slots%temp%
				sleep , 2000
				FindUIButton("TransferCommand",,,,,,,0)
				if (FoundItem==1) {
					FindUIButton("ApplyGray",,,,,,,0)
					if (FoundItem==1) {
						Transfer2("Robot2",s,ss,d,ds,0,Blade)
						WaitReady(300000)
						FindUIButton("Apply",,,,,,,0)
						if (FoundItem==1) {
							ClickUIButton("Apply")
						} else if (FoundItem==0) {
							Transfer("Robot2",s,ss,d,ds,0,Blade)
						}
					}
				} else if (FoundItem==0) {
					WaitReady(300000)
					Transfer("Robot2",s,ss,d,ds,0,Blade)
				}
			}
continuetransR2:
			abra = 0 
		}
		i += 1
		if (R2port%i%==empty) {
			s := d
			d := R2port1
			ss := ds
			ds := R2slots1
			sleep , 2000
			FindUIButton("TransferCommand",,,,,,,0)
			if (FoundItem==1) {
				FindUIButton("ApplyGray",,,,,,,0)
				if (FoundItem==1) {
					Transfer2("Robot2",s,ss,d,ds,0,Blade)
					WaitReady(300000)
					FindUIButton("Apply",,,,,,,0)
						if (FoundItem==1) {
							ClickUIButton("Apply")
						} else if (FoundItem==0) {
							Transfer("Robot2",s,ss,d,ds,0,Blade)
						}
				}
			} else if (FoundItem==0) {
				WaitReady(300000)
				Transfer("Robot2",s,ss,d,ds,0,Blade)
			}
		}
	}
}

Appr() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	Purp1 = get
	Purp2 = put
	Purp3 = gtx
	Purp4 = gtr
	Purp5 = ptx
	Purp6 = ptr
	Purp7 = taught
	if (R1Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot1",Speed)
	}
	i = 1
	j = 1
	While (R1port%i%<>empty) {
		j = 1
		While (Purp%j%<>empty) {
			s := R1port%i%
			p := Purp%j%
			sleep , 2000
			WaitReady(300000)
			Approach("Robot1",s,R1slots%i%,p,Blade)
			j += 1
		}
		i += 1
	}
	
	if (R2Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot2",Speed)
	}
	i = 1
	j = 1
	While (R2port%i%<>empty) {
		j = 1
		While (Purp%j%<>empty) {
			s := R2port%i%
			p := Purp%j%
			sleep , 2000
			WaitReady(300000)
			Approach("Robot2",s,R2slots%i%,p,Blade)
			j += 1
		}
		i += 1
	}
}

Mpre() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	ModeTerm()
	if (R1Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot1",Speed)
	}
	i = 1
	j = 1
	Go = 1
	if ((R1port0 == 1) AND (R2port0 == 0)) OR ((R2port0 == 1) AND (R1port0 == 0)) {
		Go = 0
	}
	While (R1port%i%<>empty) {
		j = 1
		s := R1port%i%
		sleep , 2000
		WaitReady(300000)
		MovPrePos("Robot1",R1port%i%,R1slots%i%,Go,Blade)
		WaitRes0(5000)
		ClickUIButton("Back")
		i += 1
	}
	
	if (R2Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot2",Speed)
	}
	i = 1
	j = 1
	While (R2port%i%<>empty) {
		j = 1
		s := R2port%i%
		sleep , 2000
		WaitReady(300000)
		MovPrePos("Robot2",R2port%i%,R2slots%i%,Go,Blade)
		WaitRes0(5000)
		ClickUIButton("Back")
		i += 1
	}
}

Maps() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	i = 1
	j = 1
	While (R1port%i%<>empty) {
		j = 1
		s := R1port%i%
		MsgBox , 4 , , %i% Map("Robot1",%s%,) ?
		ifmsgbox , Yes
			Map("Robot1",s)
		i += 1
	}
	
	i = 1
	j = 1
	While (R2port%i%<>empty) {
		j = 1
		s := R2port%i%
		MsgBox , 4 , , %i% Map("Robot2",%s%,) ?
		ifmsgbox , Yes
			Map("Robot2",s)
		i += 1
	}
}

Tmo() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	if (R1Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot1",Speed)
	}
	i = 1
	While (R1port%i%<>empty) {
		s := R1port%i%
		sleep , 1000
		WaitReady(300000)
		if (i<>1) {
			TestMotion("Robot1",s,R1slots%i%,"put",Stepping,Blade)
			PerformTestMotion("put")
		}
		TestMotion("Robot1",s,R1slots%i%,"get",Stepping,Blade)
		PerformTestMotion("get")
		i += 1
	}
	if (R1Port1<>empty) {
		TestMotion("Robot1",R1Port1,R1slots1,"put",Stepping,Blade)
		PerformTestMotion("put")
	}
	
	if (R2Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot2",Speed)
	}
	i = 1
	While (R2port%i%<>empty) {
		s := R2port%i%
		sleep , 1000
		WaitReady(300000)
		if (i<>1) {
			TestMotion("Robot2",s,R2slots%i%,"put",Stepping,Blade)
			PerformTestMotion("put")
		}
		TestMotion("Robot2",s,R2slots%i%,"get",Stepping,Blade)
		PerformTestMotion("get")
		i += 1
	}
	if (R2Port1<>empty) {
		TestMotion("Robot2",R2Port1,R2slots1,"put",Stepping,Blade)
		PerformTestMotion("put")
	}
}

GTSShipping() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	ModePhtm()
	if (R1Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot1",Speed)
	}
	i = 1
	While (R1port%i%<>empty) {
		s := R1port%i%
		sleep , 5000
		WaitReady(300000)
		sleep , 1000
		Approach("Robot1",s,R1slots%i%,"get",Blade)
		sleep , 1000
		WaitReady(300000)
		sleep , 1000
		Shipping("Robot1")
		i += 1
	}
	
	if (R2Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot2",Speed)
	}
	i = 1
	While (R2port%i%<>empty) {
		s := R2port%i%
		sleep , 5000
		WaitReady(300000)
		sleep , 1000
		Approach("Robot2",s,R2slots%i%,"get",Blade)
		sleep , 1000
		WaitReady(300000)
		sleep , 1000
		Shipping("Robot2")
		i += 1
	}
}

MPREShippingMPRE() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	if (R1Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot1",Speed)
	}
	i = 1
	While (R1port%i%<>empty) {
		s := R1port%i%
		WaitReady(300000)
		sleep , 1000
		ModeTerm()
		sleep , 1000
		MovPrePos("Robot1",R1port%i%,R1slots%i%,1,Blade)
		WaitRes0(50000)
		ClickUIButton("Misc",,,720,150)
		ClickUIButton("Robot1")
		WaitReady(300000)
		sleep , 1000
		Shipping("Robot1")
		WaitReady(300000)
		sleep , 1000
		ModeTerm()
		sleep , 1000
		MovPrePos("Robot1",R1port%i%,R1slots%i%,1,Blade)
		WaitRes0(50000)
		ClickUIButton("Misc",,,720,150)
		ClickUIButton("Robot1")
		i += 1
	}
	
	if (R2Port1<>empty) {
		WinActivate , ahk_class SunAwtFrame
		setspd("Robot2",Speed)
	}
	i = 1
	While (R2port%i%<>empty) {
		s := R2port%i%
		WaitReady(300000)
		sleep , 1000
		ModeTerm()
		sleep , 1000
		MovPrePos("Robot2",R2port%i%,R2slots%i%,1,Blade)
		WaitRes0(50000)
		ClickUIButton("Misc",,,720,150)
		ClickUIButton("Robot2")
		WaitReady(300000)
		sleep , 1000
		Shipping("Robot2")
		WaitReady(300000)
		sleep , 1000
		ModeTerm()
		sleep , 1000
		MovPrePos("Robot2",R2port%i%,R2slots%i%,1,Blade) ;xyz
		WaitRes0(50000)
		ClickUIButton("Misc",,,720,150)
		ClickUIButton("Robot2")
		i += 1
	}
}

ChoosePortsGui() {
	global
	i = 1
	Loop {
		R1Slots%i%=1
		R2Slots%i%=1
		i += 1
		if (i>=12) {
			break
		}
	}
	Blade := "1"
	BatchMode := 0
	BatchRun := 0
	AlignToo := 0
	speed := "normalspeed"
	stepping := "normalstepped"
	EnableWaferDropDetection:="Disabled"
	Gui , 1:Default
	Gui , Add , Button , gButtonSim , SetupSimLoadPorts
	Gui , Add , Text , , Select (ctrl+click) Ports available in FICGUI
	Gui , Add , Text , , R1
	Gui , Add , ListBox, vR1Ports Multi r11 w50, P1|P2|P3|P4|U1|U2|U3|U4|U5|LLA|LLB
	Gui , Add , Button , w110 gButtonOK , Select
	Gui , Add , Text , x70 y62 , R2
	Gui , Add , ListBox, vR2Ports Multi r11 w50, P1|P2|P3|P4|U1|U2|U3|U4|U5|LLA|LLB
	Gui , Add , Text , x140 y52, Selected Ports
	Gui , Add , Text , x140 y67, Double Click to Edit Slots
	Gui , Add , Listview , r17 w140 vListRPS gListRPS x140 y80, #|Robot|Port|Slots
	
	Gui, Add, GroupBox, ys w200 h500, Batchable -->
	Gui , Add , Radio , x320 y40 gRadioSpeedSlow, Slow
	Gui , Add , Radio , gRadioSpeedMedium, Medium
	Gui , Add , Radio , Checked gRadioSpeedNormal, Normal
	Gui , Add , Checkbox , gCheckboxAlign , Transfer w/ Align
	Gui , Add , Radio , Checked gRadioBlade1, Blade1
	Gui , Add , Radio , gRadioBlade2, Blade2
	Gui , Add , Button , w150 gButtonTransfer , Transfer
	Gui , Add , Button , w100 gButtonDisabled , Disabled
	Gui , Add , Button , w100 gButtonSniffCurrentPutSlot , SniffCurrentPutSlot
	Gui , Add , Button , w100 gButtonSniffBelowPutSlot , SniffBelowPutSlot
	Gui , Add , Button , w100 gButtonSniffBothSlots , SniffBothSlots
	Gui , Add , Button , w150 gButtonApproach , Approach
	Gui , Add , Button , w150 gButtonMovPrePos , MovPrePos
	Gui , Add , Button , w150 gButtonMap , Map
	Gui , Add , Button , w150 gButtonGTStoShipping , GTS-Shipping
	Gui , Add , Button , w150 gButtonMPREtoShippingtoMPRE , MPRE-Shipping-Mpre
	Gui , Add , Button , w150 gButtonTestMotion , TestMotion
	Gui , Add , Radio , gRadioSteppingSlow , SlowStepped
	Gui , Add , Radio , checked gRadioSteppingNormal , NormalStepped
	Gui , Add , Radio , gRadioSteppingCont , Continuous
	Gui , Add , Button , gButtonPhtm , Mode,Phtm
	Gui , Add , Button , gButtonTerm, Mode,Term
	Gui , Add , CheckBox , ys y40 Checked0 gCheckboxBATCH , BATCH MODE
	Gui , Add , Text , , Toggling <Batchable> buttons while BATCH MODE is checked will add them to Batch List below
	Gui , Add , Listview , r15 w600 vListBatch gListBatch, #|Command|Robot/Ports/Slots|Blade|Speed|Align?|Stepping|EnableWaferDropDetection
	Gui , Add , Button , gButtonBatchGo , Perform Batch Processing
	Gui , Add , Button , gButtonClearList , Clear List
	LV_Delete()
	BatchIndex := 1
	Gui , Show
	return
	
ButtonClearList:
	Gui, ListView, ListBatch	;test
	LV_Delete()
	clear := 1
	Loop 
	{
		if (BatchCommand%clear%==empty)
		{
			BatchIndex := 1
			return
		}
		BatchCommand%clear%:=empty
		BatchBlade%clear%:=empty
		BatchSpeed%clear%:=empty
		BatchAlignToo%clear%:=empty
		BatchStepping%clear%:=empty
		BatchEnableWaferDropDetection%clear%:=empty
		clear += 1
	}
	BatchIndex := 1
	return
ButtonDisabled:
	Command := "EnableWaferDropDetection"
	EnableWaferDropDetection:="Disabled"
	EnableWaferDropDetection(EnableWaferDropDetection)
	return
ButtonSniffCurrentPutSlot:
	Command := "EnableWaferDropDetection"
	EnableWaferDropDetection:="SniffCurrentPutSlot"
	EnableWaferDropDetection(EnableWaferDropDetection)
	return
ButtonSniffBelowPutSlot:
	Command := "EnableWaferDropDetection"
	EnableWaferDropDetection:="SniffBelowPutSlot"
	EnableWaferDropDetection(EnableWaferDropDetection)
	return
ButtonSniffBothSlots:
	Command := "EnableWaferDropDetection"
	EnableWaferDropDetection:="SniffBothSlots"
	EnableWaferDropDetection(EnableWaferDropDetection)
	return
RadioSteppingSlow:
	Stepping := "slowstepped"
	return
RadioSteppingNormal:
	Stepping := "normalstepped"
	return
RadioSteppingCont:
	Stepping := "continuous"
	return
ButtonBatchGo:
	BatchGo()
	return
ButtonTestMotion:
	Command := "Tmo"
	Tmo()
	return
CheckboxBATCH:
	if (BatchMode==0) {
		BatchMode:=1
		return
	}
	if (BatchMode==1) {
		BatchMode:=0
		return
	}
	return
ListBatch:
;	if A_GuiEvent = DoubleClick
;	x := A_EventInfo
;	{
;		LV_GetText(BatchNum , x, 1)
;		LV_GetText(BatchNumU , x+1, 1)
;		if (BatchNumU == BatchNum) {
;			LV_Delete(x+1)
;		}
;		LV_Delete(x)
;		LV_GetText(BatchNumD , x-1, 1)
;		if (BatchNumD == BatchNum) {
;			LV_Delete(x-1)
;		}
;	}
	return
RadioSpeedSlow:
	speed := "slowspeed"
	return
RadioSpeedMedium:
	speed := "mediumspeed"
	return
RadioSpeedNormal:
	speed := "normalspeed"
	return
ButtonGTStoShipping:
	Command := "GTSShipping"
	GTSShipping()
	return
ButtonMPREtoShippingtoMPRE:
	Command := "MPREShippingMPRE"
	MPREShippingMPRE()
	return
ButtonOK:
	Gui , Submit , NoHide
	StringSplit, R1port, R1Ports , |
	Loop {
		x := R1port0+A_Index
		if (R1port%x% <> empty) {
			R1port%x% := empty
		} else {
			Break
		}
	}
	StringSplit, R2port, R2ports , |
	Loop {
		x := R2port0+A_Index
		if (R2port%x% <> empty) {
			R2port%x% := empty
		} else {
			Break
		}
	}
	Gui, ListView, ListRPS	;test
	LV_Delete()
	if (R1port0 <> 0) {
		i = 1
		Loop {
			x := R1port0
			LV_Add("",i,"R1",R1port%i%,R1Slots%i%)
			i += 1
			if (i>x) {
				break
			}
		}
	}
	if (R2port0 <> 0) {
		LV_Add("","-","-","-","-")
		i = 1
		Loop {
			x := R2port0
			LV_Add("",i,"R2",R2port%i%,R2Slots%i%)
			i += 1
			if (i>x) {
				break
			}
		}
	}
	return
ListRPS:
	if A_GuiEvent = DoubleClick
	selectedrow := A_EventInfo			;strange - might be a problem, if statement line
	{
		LV_GetText(RowNum , A_EventInfo, 1)
		LV_GetText(RobotNum , A_EventInfo, 2)
		LV_GetText(PortNum , A_EventInfo, 3)
		LV_GetText(SlotNum, A_EventInfo, 4)
		Gui , 2:Default
		Gui , Add , Edit , r1 w50 vEditSlots , %SlotNum%
		Gui , Add , Button , gButtonSlots , Change #Slots
		Gui , Show
		Gui , 1:Default
	}
	return
	
ButtonSlots:
	Gui , 2:Default
	Gui, Submit , NoHide
	if (RobotNum == "R1") {
		Gui , 2:Default
		R1Slots%RowNum% := EditSlots
		Gui , Destroy
		Gui , 1:Default
		LV_Modify(selectedrow,"", RowNum, RobotNum, PortNum, R1Slots%RowNum%)
	}
	if (RobotNum == "R1") {
		Gui , 2:Default
		R2Slots%RowNum% := EditSlots
		Gui , Destroy
		Gui , 1:Default
		LV_Modify(selectedrow,"", RowNum, RobotNum, PortNum, R2Slots%RowNum%)
	}
	Gui , 1:Default
	return

GuiClose:
	ExitApp
ButtonTransfer:
	Command := "Trans"
	Trans()
	return
ButtonApproach:
	Command := "Appr"
	Appr()
	return
ButtonSim:
	setupFIsim()
	return
ButtonMap:
	Command := "Maps"
	Maps()
	return
ButtonMovPrePos:
	Command := "Mpre"
	Mpre()
	return
ButtonPhtm:
	Command := "ModePhtm"
	ModePhtm()
	return
ButtonTerm:
	Command := "ModeTerm"
	ModeTerm()
	return
CheckboxAlign:
	if (AlignToo==0) {
		AlignToo:=1
		return
	}
	if (AlignToo==1) {
		AlignToo:=0
		return
	}
	return
RadioBlade1:
	Blade:="1"
	return
RadioBlade2:
	Blade:="2"
	return
}

ModePhtm() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	WinActivate , ahk_class SunAwtFrame
	Imagesearch , fx , fy , 0 , 0 , 1043 , 200 , %A_ScriptDir%\Resources\FIROBOT1MAIN.PNG
	if (errorlevel == 0) {
		ClickUIButton("Ready")
		goto ModePhtmJump
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton("robot1")
ModePhtmJump:
	WaitReady(10000)
	ClickUIButton("Terminal")
	ClickUIButton("field",,450)
	Send {Raw} <122:mode,phtm>??
	Send {enter}
	sleep , 200
}

ModeTerm() {
	global
	if (BatchMode == 1) AND (BatchRun == 0) {
		BatchSet()
		return
	}
	WinActivate , ahk_class SunAwtFrame
	WinActivate , ahk_class SunAwtFrame
	if (Robot="Robot1") {
		Imagesearch , fx , fy , 0 , 0 , 1043 , 200 , %A_ScriptDir%\Resources\FIROBOT1MAIN.PNG
		if (errorlevel == 0) {
			ClickUIButton("Ready")
			goto ModeTermJump
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton("Robot1")
ModeTermJump:
	WaitReady(10000)
	ClickUIButton("Terminal")
	ClickUIButton("field")
	Send {Raw} <122:mode,term>??
	Send {enter}
	sleep , 200
}

WaitStep(Purpose, Timeout) {
	sleep , 200
	if (purpose = "get") {
		img = *10 %A_ScriptDir%\Resources\stepgettest.png
	} else if (purpose = "put") {
		img = *10 %A_ScriptDir%\Resources\stepputtest.png
	}
	ImageWait(Foundx , Foundy , 0 , 0 , 1043 , 790 , img, Timeout )
}

TestMotion(Robot,Station,Slot,Purpose,Stepping,Blade="1") {				; testing
	WinActivate , ahk_class SunAwtFrame
	if (Robot="Robot1") {
		FindUIButton("FIROBOT1MAIN",,,200,,800,100,0)
		if (FoundItem == 1) {
			ClickUIButton("Ready")
		}
	} else if (Robot="Robot2") {
		FindUIButton("FIROBOT2MAIN",,,200,,800,100,0)
		if (FoundItem == 1) {
			ClickUIButton("Ready")
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
	ClickUIButton("Service",700)
	ClickUIButton("Calibration",700)
	ClickUIButton("Advanced",700)
	ClickUIButton("TestMotion",400,200,700,400)
	ClickUIButton("Pod",500,300)
	ClickPort(Station)
	ClickUIButton("1",500,300)
	ClickSlot(Slot)
	ClickUIButton("get",500,300)
	ClickUIButton(Purpose)
	ClickUIButton("Blade",500,300,,,100)
	if (Blade="1") {
		ClickUIButton("Blade1",750,500)
	} else if (Blade="2") {
		ClickUIButton("Blade2",750,500)
	}
	ClickUIButton("Type",500,300,,,100)
	sleep , 1000
	ClickUIButton(Stepping,500,400)
	ClickUIButton("Apply",500,300)
}

PerformTestMotion(Purpose) {
	global FoundItem
	FindUIButton("runatestofthecalibration",,,,,,,0)
	if (FoundItem==0) {
		ClickUIButton("testofthecalibration",,,,150)
		ClickUIButton("OK",150,30,400,128)
		goto perfjump
	}
	ClickUIButton("runatestofthecalibration")
	ClickUIButton("yes",150,30,400,128)
perfjump:
	WaitReady(10000)
	sleep , 1000
	if (purpose = "get") {
		ClickUIButton("confirmthatthereisaregularwafer",,,900,128)
		ClickUIButton("yes",150,30,400,128)
	}
	if (purpose = "put") {
		ClickUIButton("isempty",,,900,150)
		FindUIButton("yes",,,150,30,400,128,0)
		if (FoundItem==1) {
			ClickUIButton("yes",150,30,400,128)
		}
		ClickUIButton("ok",150,30,400,128)
	}
looptm:
		WaitStep(Purpose,100000)
		FindUIButton("stepstorun0",,,200,300,450,400,0)
		if (FoundItem == 0) {
			ClickUIButton("stepstorun",200,300,450,400,-150)
			ClickUIButton("stepstorun",200,300,450,400,300)
			goto looptm
		} else if (FoundItem == 1) {
			ClickUIButton("done",0,300,300)
		}
}

MovPrePos(Robot,Station,Slot,Go,Blade="1") {
	WinActivate , ahk_class SunAwtFrame
	if (Robot="Robot1") {
		FindUIButton("FIROBOT1MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			ClickUIButton("Ready")
			goto MovPrePosJump
		}
	} else if (Robot="Robot2") {
		FindUIButton("FIROBOT2MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			ClickUIButton("Ready")
			goto MovPrePosJump
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
MovPrePosJump:
	WaitReady(10000)
	ClickUIButton("Terminal")
	ClickUIButton("field",,450)
	Send {Raw}<122:movprepos,
	if (Robot = "Robot1") {
		Send {Raw}R1:
	} else if (Robot = "Robot2") {
		Send {Raw}R2:
	}
	if (Blade = "1") {
		Send {Raw}B1,
	} else if (Blade = "2") {
		Send {Raw}B2,
	}
	Send %Station%:%Slot%>??
	if (Go) {
		Send {enter}
	}
}

Shipping(Robot) {
	WinActivate , ahk_class SunAwtFrame
	if (Robot="Robot1") {
		FindUIButton("FIROBOT1MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			ClickUIButton("Ready")
			goto ShippingJump
		}
	} else if (Robot="Robot2") {
		FindUIButton("FIROBOT2MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			ClickUIButton("Ready")
			goto ShippingJump
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
ShippingJump:
	ClickUIButton("Service")
	ClickUIButton("Movetoshippingposition")
	ClickUIButton("OK")
}
	
Map(Robot,Pod) {
	WinActivate , ahk_class SunAwtFrame
	if (Robot="Robot1") {
		FindUIButton("FIROBOT1MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			ClickUIButton("Ready")
			goto MapJump
		}
	} else if (Robot="Robot2") {
		FindUIButton("FIROBOT2MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			ClickUIButton("Ready")
			goto MapJump
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
MapJump:
	ClickUIButton("Map")
	ClickUIButton("Pod")
	ClickPort(Pod)
	ClickUIButton("Apply")
	ClickUIButton("Close")
}

Transfer(Robot,Source,SourceSlot,Dest,DestSlot,Align=0,Blade="1") {
	WinActivate , ahk_class SunAwtFrame
	global FoundItem := 0
	if (Robot="Robot1") {
		FindUIButton("FIROBOT1MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			FindUIButton("TransferCommand",,,550,300,800,400,0)
			if (FoundItem == 1) {
				goto TransferJump
			}
			ClickUIButton("Ready",50,170,260,250)
			ClickUIButton("Transfer",530,350,770,440)
			goto TransferJump
		}
	} else if (Robot="Robot2") {
		FindUIButton("FIROBOT2MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			FindUIButton("TransferCommand",,,550,300,800,400,0)
			if (FoundItem == 1) {
				goto TransferJump
			}
			ClickUIButton("Ready",50,170,260,250)
			ClickUIButton("Transfer",530,350,770,440)
			goto TransferJump
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
	ClickUIButton("Transfer",530,350,770,440)
TransferJump:
	ClickUIButton("Destination",850,,1000,,,60)
	ClickPort(Dest,750)
	ClickUIButton("Destination",850,,1000,,,120) ;SLOT
	ClickSlot(DestSlot,500,300)
	ClickUIButton("Source",700,,850,,,60)
	ClickPort(Source,700)
	ClickUIButton("Source",700,,850,,,120)
	ClickSlot(SourceSlot,500,300)
	if (Align == 1) {
		DoAlign := 0
		FindUIButton("AlignTransfer",,,,,,,0,1)
		if (FoundItem == 1) {
			ClickUIButton("AlignTransfer",500,550)
			DoAlign := 1
			ClickUIButton("Blade",500,500,,,200)
		}
		FindUIButton("AlignTransfer2",,,,,,,0,1)
		if (FoundItem == 1) {
			DoAlign := 1
		}
		if (DoAlign<>1) {
			;MsgBox Could not find %UIButton%
			;msgbox 4 , , Continue?
			;ifmsgbox , No
			;	ExitApp
		}
	}
	if (Align == 0) {
		DoAlign := 0
		FindUIButton("AlignTransfer",,,,,,,0,1)
		if (FoundItem == 1) {
			DoAlign := 1
		}
		FindUIButton("AlignTransfer2",,,,,,,0,1)
		if (FoundItem == 1) {
			ClickUIButton("AlignTransfer2",500,550)
			DoAlign := 1
			ClickUIButton("Blade",500,500,,,200)
		}
		if (DoAlign<>1) {
			;MsgBox Could not find %UIButton%
			;msgbox 4 , , Continue?
			;ifmsgbox , No
			;	ExitApp
		}
	}
	ClickUIButton("BLADE",500,500,,,100)
	ClickUIButton(Blade,820,570)
	ClickUIButton("Apply",500,650)
}

Transfer2(Robot,Source,SourceSlot,Dest,DestSlot,Align=0,Blade="1") {
	WinActivate , ahk_class SunAwtFrame
	global FoundItem := 0
	if (Robot="Robot1") {
		FindUIButton("FIROBOT1MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			FindUIButton("TransferCommand",,,550,300,800,400,0)
			if (FoundItem == 1) {
				goto Transfer2Jump
			}
			ClickUIButton("Ready",50,170,260,250)
			ClickUIButton("Transfer",530,350,770,440)
			goto Transfer2Jump
		}
	} else if (Robot="Robot2") {
		FindUIButton("FIROBOT2MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			FindUIButton("TransferCommand",,,550,300,800,400,0)
			if (FoundItem == 1) {
				goto Transfer2Jump
			}
			ClickUIButton("Ready",50,170,260,250)
			ClickUIButton("Transfer",530,350,770,440)
			goto Transfer2Jump
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
	ClickUIButton("Transfer",530,350,770,440)
Transfer2Jump:
	ClickUIButton("Destination",850,,1000,,,60)
	Sleep , 500
	ClickPort(Dest,750)
	ClickUIButton("Destination",850,,1000,,,120) ;SLOT
	ClickSlot(DestSlot,500,300)
	ClickUIButton("Source",700,,850,,,60)
	Sleep , 500
	ClickPort(Source,700)
	ClickUIButton("Source",700,,850,,,120)
	ClickSlot(SourceSlot,500,300)
	if (Align == 1) {
		DoAlign := 0
		FindUIButton("AlignTransfer",,,,,,,0,1)
		if (FoundItem == 1) {
			ClickUIButton("AlignTransfer",500,550)
			DoAlign := 1
			ClickUIButton("Blade",500,500,,,200)
		}
		FindUIButton("AlignTransfer2",,,,,,,0,1)
		if (FoundItem == 1) {
			DoAlign := 1
		}
		if (DoAlign<>1) {
			;MsgBox Could not find %UIButton%
			;msgbox 4 , , Continue?
			;ifmsgbox , No
		;		ExitApp
		}
	}
	if (Align == 0) {
		DoAlign := 0
		FindUIButton("AlignTransfer",,,,,,,0,1)
		if (FoundItem == 1) {
			DoAlign := 1
		}
		FindUIButton("AlignTransfer2",,,,,,,0,1)
		if (FoundItem == 1) {
			ClickUIButton("AlignTransfer2",500,550)
			DoAlign := 1
			ClickUIButton("Blade",500,500,,,200)
		}
		if (DoAlign<>1) {
		;	MsgBox Could not find %UIButton%
	;		msgbox 4 , , Continue?
;			ifmsgbox , No
;				ExitApp
		}
	}
	ClickUIButton("BLADE",500,500,,,100)
	ClickUIButton(Blade,820,570)
}

Teach(Robot,Station,Slot,Blade) {						;; test
	WinActivate , ahk_class SunAwtFrame
	if (Robot="Robot1") {
		Imagesearch , fx , fy , 0 , 0 , 1043 , 200 , %A_ScriptDir%\Resources\FIROBOT1MAIN.PNG
		if (errorlevel == 0) {
			ClickUIButton("Ready")
			goto TeachJump
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
TeachJump:
	ClickUIButton("Teach")
	ClickUIButton("Pod")
	ClickPort(Station)
	ClickUIButton("1")
	if (Station <> "P1") AND (Station <> "P2") AND (Station <> "Pod1") AND (Station <> "Pod2") AND (Station <> "P3") AND (Station <> "P4") AND (Station <> "Pod3") AND (Station <> "Pod4") {
		ClickUIButton(Slot)
	} else {
		ClickSlot(Slot)
	}
	sleep , 300
	ClickUIButton("BLADE",,,,,100)
	ClickUIButton(Blade)
	ClickUIbutton("Teach")
	ClickUIbutton("save")
}

Approach(Robot,Station,Slot,Purpose,Blade="1") {
	WinActivate , ahk_class SunAwtFrame
	if (Robot="Robot1") {
		FindUIButton("FIROBOT1MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			goto ApproachJump
		}
	} else if (Robot="Robot2") {
		FindUIButton("FIROBOT2MAIN",,,,,,100,0)
		if (FoundItem == 1) {
			goto ApproachJump
		}
	}
	ClickUIButton("Misc",,,720,150)
	ClickUIButton(Robot,,,550,300)
ApproachJump:
	ClickUIButton("Approach",500)
	ClickUIButton("Pod",500)
	ClickPort(Station,400)
	ClickUIButton("1",500)
	if (Station <> "P1") AND (Station <> "P2") AND (Station <> "Pod1") AND (Station <> "Pod2") AND (Station <> "P3") AND (Station <> "P4") AND (Station <> "Pod3") AND (Station <> "Pod4") {
		ClickUIButton(Slot,400)
	} else {
		ClickSlot(Slot,400)
	}
	ClickUIButton("get",500)
	ClickUIButton(Purpose,500)
	ClickUIButton("BLADE",500,,,,100)
	ClickUIButton(Blade,500)
	ClickUIbutton("Apply",500)
}

ClickUIButton(ByRef UIButton, Ux=0, Uy=0, Lx=1043, Ly=790, xoff=0, yoff=0, retries=5) {
	FindUIButton(UIButton,Foundx,Foundy, Ux , Uy , Lx , Ly, 1, retries)
	x := Foundx + xoff
	y := Foundy + yoff
	Click %x% %y%
	sleep , 50
	global FoundItem := 0
}

FindUIButton(ByRef UIbutton, ByRef Foundx=0,ByRef Foundy=0, Ux=0, Uy=0, Lx=1043, Ly=790,StopIfNotFound=1,retries=5) {
FindUIButtonRetry:
	global FoundItem := 0
	if (UIButton = "1"){
		ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1_5.PNG
		if (ErrorLevel == 1) {	
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1_4.PNG
		}
		if (ErrorLevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1.PNG
		}
		if (ErrorLevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1_2.PNG
		}
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1_3.PNG
		}
	} else if (UIButton = "2"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\2.PNG
		if (ErrorLevel == 1) {	
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\2_1.PNG
		}
	} else if (UIButton = "ABORT-STOP"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\ABORT-STOP.PNG
    } else if (UIButton = "AlignTransfer"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\AlignTransfer.PNG
	} else if (UIButton = "smallRes0"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\smallRes0.PNG
	} else if (UIButton = "isnowretracted"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\isnowretracted.PNG
	} else if (UIButton = "AlignTransfer2"){
		ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\AlignTransfer2.PNG
    } else if (UIButton = "APPLY"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\APPLY.PNG
    } else if (UIButton = "APPLYALLCHANGES"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\APPLYALLCHANGES.PNG
	} else if (UIButton = "APPROACH"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\APPROACH.PNG
	} else if (UIButton = "BLADE"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\BLADE.PNG
    } else if (UIButton = "CLOSE"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\CLOSE_2.PNG
		if (ErrorLevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\CLOSE_3.PNG
		}
		if (ErrorLevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\CLOSE.PNG
		}
    } else if (UIButton = "CONFIGEDITOR"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\CONFIGEDITOR.PNG
    } else if (UIButton = "CONFIGURATION"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\CONFIGURATION.PNG
    } else if (UIButton = "DWS"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\DWS.PNG
    } else if (UIButton = "Destination"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\Destination.PNG
    } else if (UIButton = "Source"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\Source.PNG
    } else if (UIButton = "TransferGray"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\TransferGray.PNG
    } else if (UIButton = "ApplyGray"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\ApplyGray.PNG
    } else if (UIButton = "ENTER"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\ENTER.PNG
    } else if (UIButton = "EXIT"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\EXIT.PNG
    } else if (UIButton = "FI"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\FI.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\FI_2.PNG
		}
	} else if (UIButton = "FICISUPANDRUNNING"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\FICISUPANDRUNNING.PNG
    } else if (UIButton = "field"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\field.PNG
	} else if (UIButton = "FISETUP"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\FISETUP.PNG
	} else if (UIButton = "FIROBOT1MAIN"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\FIROBOT1MAIN.PNG
	} else if (UIButton = "FIROBOT2MAIN"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\FIROBOT2MAIN.PNG
    } else if (UIButton = "get"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\get.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\get_2.PNG
		}
	} else if (UIButton = "gotofirst"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\gotofirst.PNG
	} else if (UIButton = "gotolast"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\gotolast.PNG
    } else if (UIButton = "gtr"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\gtr.PNG
    } else if (UIButton = "gtx"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\gtx.PNG
    } else if (UIButton = "HOLD-RELEASE"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\HOLD-RELEASE.PNG
    } else if (UIButton = "HOME"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\HOME.PNG
    } else if (UIButton = "HOMEALL"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\HOMEALL.PNG
    } else if (UIButton = "INIT"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\INIT.PNG
    } else if (UIButton = "INITALL"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\INITALL.PNG
    } else if (UIButton = "LLA"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\LLA.PNG
	} else if (UIButton = "LOCAL"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\LOCAL.PNG
    } else if (UIButton = "MAIN"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\MAIN.PNG
    } else if (UIButton = "MAINTENENCE"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\MAINTENENCE.PNG
    } else if (UIButton = "MAP"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\MAP.PNG
    } else if (UIbutton = "MISC"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\MISC.PNG
    } else if (UIButton = "MOTORPOWER"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\MOTORPOWER.PNG
    } else if (UIButton = "MOVEAXIS"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\MOVEAXIS.PNG
    } else if (UIButton = "MOVETOSHIPPINGPOSITION"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\MOVETOSHIPPINGPOSITION.PNG
    } else if (UIButton = "OK"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\OK.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\OK_2.PNG
		}
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\OK_3.PNG
		}
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\OK_4.PNG
		}
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\OK_5.PNG
		}
	} else if (UIButton = "OPERPANEL"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\OPERPANEL.PNG
    } else if (UIButton = "PASSTHRU"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\PASSTHRU.PNG
    } else if (UIButton = "POD"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD_2.PNG
		}
	} else if (UIButton = "POD1"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD1.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD1_2.PNG
		}
    } else if (UIButton = "ptr"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\ptr.PNG
    } else if (UIButton = "ptx"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\ptx.PNG
    } else if (UIButton = "put"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\put.PNG
    } else if (UIButton = "READY"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\READY.PNG
    } else if (UIButton = "RESET"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\RESET.PNG
    } else if (UIButton = "RESETALL"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\RESETALL.PNG
    } else if (UIButton = "ROBOT1") OR (UIButton = "Robot1cmd") {
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\ROBOT1.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\Robot1cmd.PNG
		}
    } else if (UIButton = "ROBOT2") OR (UIButton = "Robot2cmd") {
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\ROBOT2.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\Robot2cmd.PNG
		}
    } else if (UIButton = "SERVICE"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SERVICE.PNG
    } else if (UIButton = "SETDESIREDROBOTSPEED"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SETDESIREDROBOTSPEED.PNG
    } else if (UIButton = "SSP1"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SSP1.PNG
    } else if (UIButton = "SSP2"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SSP2.PNG
    } else if (UIButton = "STOPALL"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\STOPALL.PNG
    } else if (UIButton = "SUCCESS"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SUCCESS.PNG
	} else if (UIButton = "SYSTEM"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SYSTEM.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SYSTEM_2.PNG
		}
    } else if (UIButton = "taught"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\taught.PNG
    } else if (UIButton = "TERMINAL"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\TERMINAL.PNG
    } else if (UIButton = "TRANSFER"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\TRANSFER.PNG
	} else if (UIButton = "TransferCommand"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\TransferCommand.PNG
    } else if (UIButton = "USERNAME"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\USERNAME.PNG
    } else if (UIButton = "gotofirst"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\gotofirst.PNG
	} else if (UIButton = "gotolast"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\gotolast.PNG
	} else if (UIButton = "slowspeed"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\slowspeed.PNG
	} else if (UIButton = "mediumspeed"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\mediumspeed.PNG
	} else if (UIButton = "normalspeed"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\normalspeed.PNG
	} else if (UIButton = "newcommand"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\newcommand.PNG
	} else if (UIButton = "back"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\back.PNG
	} else if (UIButton = "statr1done"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\statr1done.PNG
	} else if (UIButton = "statr2done"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\statr2done.PNG
	} else if (UIButton = "calibration"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\calibration.PNG
	} else if (UIButton = "advanced"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\advanced.PNG
	} else if (UIButton = "testmotion"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\testmotion.PNG
	} else if (UIButton = "runatestofthecalibration"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\doyouwanttorunatestofthecalibration.PNG
	} else if (UIButton = "testofthecalibration"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\testofthecalibration.PNG
	} else if (UIButton = "confirmthatthereisaregularwafer"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\confirmthatthereisaregularwafer.PNG
	} else if (UIButton = "isempty"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\isempty.PNG
	} else if (UIButton = "type"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\type.PNG
	} else if (UIButton = "stepstorun0"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\stepstorun0.PNG
	} else if (UIButton = "stepstorun"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\stepstorun.PNG
	} else if (UIButton = "done"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\done.PNG
	} else if (UIButton = "slowstepped"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\slowstepped.PNG
	} else if (UIButton = "normalstepped"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\normalstepped.PNG
	} else if (UIButton = "Continuous"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\Continuous.PNG
	} else if (UIButton = "yes"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\yes.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\yes_2.PNG
		}
	} else if (UIButton = "blade1"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\blade1.PNG
	} else if (UIButton = "blade2"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\blade2.PNG
	} else if (UIButton = "enablewaferdropdetection"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\enablewaferdropdetection.PNG
	} else if (UIButton = "disabled"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\disabled.PNG
	} else if (UIButton = "sniffcurrentputslot"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\sniffcurrentputslot.PNG
	} else if (UIButton = "sniffbelowputslot"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\sniffslotbelowputslot.PNG
	} else if (UIButton = "sniffbothslots"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\sniffbothslots.PNG
	} else if (UIButton = "teach"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\teach.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\teach_2.PNG
		}
	} else if (UIButton = "save"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\save.PNG
	} else if (UIButton = "stepgettest"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\stepgettest.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\stepgettest_2.PNG
		}
	} else if (UIButton = "stepputtest"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\stepputtest.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\stepputtest_2.PNG
		}
	}	else if (UIButton = "step"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\step.PNG
	}
	if (ErrorLevel ==2) {
		MsgBox Could not conduct the search for %UIbutton%
		ExitApp
	} else if (Errorlevel == 1) {
		if (retries<>0) {
			retries -= 1
			sleep , 200
			goto FindUIButtonRetry
		}
		if (StopIfNotFound==1) {
			MsgBox Could not find %UIButton%
			msgbox 4 , , Continue?
			ifmsgbox , No
				ExitApp
		}
	}
	if (Errorlevel == 0) {
		global FoundItem := 1
	}
}

ClickPort(UIButton, Ux=0, Uy=0, Lx=1043, Ly=790, retries=5) {
ClickPortRetry:
	if (UIButton = "POD1") OR (UIButton = "P1"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD1.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD1_2.PNG
		}
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD1cmd.PNG
		}
	} else if (UIButton = "POD2") OR (UIButton = "P2"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD2.PNG
	} else if (UIButton = "POD3") OR (UIButton = "P3"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD3.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD3_2.PNG
		}
	} else if (UIButton = "POD4") OR (UIButton = "P4"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\POD4.PNG
	} else if (UIButton = "PASSTHRU"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\PASSTHRU.PNG
	} else if (UIButton = "LLA"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\LLA.PNG
	} else if (UIButton = "LLB"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\LLB.PNG
	} else if (UIButton = "DWS"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\DWS.PNG
	} else if (UIButton = "PASSTHRU"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\PASSTHRU.PNG
	} else if (UIButton = "SSP1"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SSP1.PNG
    } else if (UIButton = "SSP2"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\SSP2.PNG
	} else if (UIButton = "U1"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U1.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U1_2.PNG
		}
    } else if (UIButton = "U2"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U2.PNG
	} else if (UIButton = "U3"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U3.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U3_2.PNG
		}
	} else if (UIButton = "U4"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U4.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U4_2.PNG
		}
    } else if (UIButton = "U5") OR (UIButton = "Bento"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U5.PNG
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\U5_2.PNG
		}
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\Bento.PNG
		}
	}
	global check
	global error
	xyz = 1
	if (ErrorLevel ==2) {
		MsgBox Could not conduct the search for %UIbutton%
		if (check == 1) {
			error = 1
			Foundx=0
			Foundy=0
		}
		ExitApp
	} else if (Errorlevel == 1) {
		if (retries<>0) {
			retries -= 1
			sleep , 200
			goto ClickPortRetry
		}
		if (xyz==1) {
			MsgBox Could not find %UIButton%
			xyz = xyz + 1
		}
		if (check == 1) {
			error = 1
			Foundx=0
			Foundy=0
		}
		msgbox 4 , , Continue?
		ifmsgbox , No
			ExitApp
	}
	Click %Foundx% , %Foundy%
	Foundx = 0
	Foundy = 0
	global FoundItem := 0
}

ClickSlot(UIbutton, Ux=0, Uy=0, Lx=1043, Ly=790, retries=5) {
ClickSlotRetry:
    ImageSearch , Foundx , Foundy , 900 , 450 , Lx , Ly , *10 %A_ScriptDir%\Resources\gotofirst.PNG
	if (ErrorLevel == 0) {
		ClickUIButton("gotofirst")
	}
	if (UIButton = 1) OR (UIButton = "1"){
		ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1_5.PNG
		if (ErrorLevel == 1) {	
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1_4.PNG
		}
		if (E5rrorLevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1.PNG
		}
		if (ErrorLevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1_2.PNG
		}
		if (Errorlevel == 1) {
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\1_3.PNG
		}
	} else if (UIButton = 2) OR (UIButton = "2"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\2.PNG
	} else if (UIButton = 3) OR (UIButton = "3"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\3.PNG
    } else if (UIButton = 4) OR (UIButton = "4"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\4.PNG
    } else if (UIButton = 5) OR (UIButton = "5"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\5.PNG
    } else if (UIButton = 6) OR (UIButton = "6"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\6.PNG
    } else if (UIButton = 7) OR (UIButton = "7"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\7.PNG
    } else if (UIButton = 8) OR (UIButton = "8"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\8.PNG
    } else if (UIButton = 9) OR (UIButton = "9"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\9.PNG
    } else if (UIButton = 10) OR (UIButton = "10"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\10.PNG
    } else if (UIButton = 11) OR (UIButton = "11"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\11.PNG
    } else if (UIButton = 12) OR (UIButton = "12"){
        ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\12.PNG
	} else {
		ImageSearch , Foundx , Foundy , 900 , 450 , Lx , Ly , *10 %A_ScriptDir%\Resources\bigdown.PNG
        Click %Foundx% , %Foundy%
		Foundx = 0
		Foundy = 0
		if (UIButton = 13) OR (UIButton = "13"){
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\13.PNG
		} else if (UIButton = 14) OR (UIButton = "14"){
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\14.PNG
		} else if (UIButton = 15) OR (UIButton = "15"){
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\15.PNG
		} else if (UIButton = 16) OR (UIButton = "16"){
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\16.PNG
		} else if (UIButton = 17) OR (UIButton = "17"){
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\17.PNG
		} else if (UIButton = 18) OR (UIButton = "18"){
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\18.PNG
		} else if (UIButton = 19) OR (UIButton = "19"){
			ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\19.PNG
		} else {
			ImageSearch , Foundx , Foundy , 900 , 450 , Lx , Ly , *10 %A_ScriptDir%\Resources\bigdown.PNG
			Click %Foundx% , %Foundy%
			Foundx = 0
			Foundy = 0
			if (UIButton = 20) OR (UIButton = "20"){
				ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\20.PNG
			} else if (UIButton = 21) OR (UIButton = "21"){
				ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\21.PNG
			} else if (UIButton = 22) OR (UIButton = "22"){
				ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\22.PNG
			} else if (UIButton = 23) OR (UIButton = "23"){
				ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\23.PNG
			} else if (UIButton = 24) OR (UIButton = "24"){
				ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\24.PNG
			} else if (UIButton = 25) OR (UIButton = "25"){
				ImageSearch , Foundx , Foundy , Ux , Uy , Lx , Ly , *10 %A_ScriptDir%\Resources\25.PNG
			} else {
				return
			}
		}
	}
	global check
	global error
	xyz = 1
	if (ErrorLevel ==2) {
		MsgBox Could not conduct the search for %UIbutton%
		if (check == 1) {
			error = 1
			Foundx=0
			Foundy=0
		}
		ExitApp
	} else if (Errorlevel == 1) {
		if (retries<>0) {
			retries -= 1
			sleep , 200
			goto ClickSlotRetry
		}
		if (xyz==1) {
			MsgBox Could not find %UIButton%
			xyz = xyz + 1
		}
		if (check == 1) {
			error = 1
			Foundx=0
			Foundy=0
		}
		msgbox 4 , , Continue?
		ifmsgbox , No
			ExitApp
	}
	Click %Foundx% , %Foundy%
	Foundx = 0
	Foundy = 0
	global FoundItem := 0
}

setupFIsim() {
	WinActivate , FI Simulator
		SendMessage, 0x1330, 2,, SysTabControl321, FI Simulator
NextLP:
		msgbox , 4 , LoadPorts , Setup this LoadPort? (Select the correct LoadPort tab)
		ifmsgbox , Yes
			SetupLoadPorts()
		ifmsgbox , No
			return
		Control , TabRight , 1 , SysTabControl321 , FI Simulator
		goto NextLP
	sleep 1000
}

SetupLoadPorts(){
	sleep , 100
	WinActivate , FI Simulator
	ControlGet , Curr , Checked , , Button8 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button8 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button8 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button9 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button9 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button9 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button10 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button10 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button10 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button11 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button11 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button11 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button12 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button12 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button12 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button56 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button56 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button56 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button57 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button57 , FI Simulator
		if (Stat == 1) {
			ControlClick , Button57 , FI Simulator
		} else if (Stat == 0) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button58 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button58 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button58 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button59 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button59 , FI Simulator
		if (Stat == 1) {
			ControlClick , Button59 , FI Simulator
		} else if (Stat == 0) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button13 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button13 , FI Simulator
		if (Stat == 1) {
			ControlClick , Button13 , FI Simulator
		} else if (Stat == 0) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button25 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button25 , FI Simulator
		if (Stat == 1) {
			ControlClick , Button25 , FI Simulator
		} else if (Stat == 0) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button26 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button26 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button26 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button27 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button27 , FI Simulator
		if (Stat == 1) {
			ControlClick , Button27 , FI Simulator
		} else if (Stat == 0) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button28 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button28 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button28 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button33 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button33 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button33 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button34 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button34 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button34 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button35 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button35 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button35 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}
	ControlGet , Curr , Checked , , Button36 , FI Simulator
	Stat = Curr
	Loop , 10 {
		ControlGet , Stat , Checked , , Button36 , FI Simulator
		if (Stat == 0) {
			ControlClick , Button36 , FI Simulator
		} else if (Stat == 1) {
			break
		}
	}

}

Final:
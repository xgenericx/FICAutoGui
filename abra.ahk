#InstallKeybdHook
#Persistent
toggle = 0
#MaxThreadsPerHotkey 2

!s::
WinActivate , FI Simulator
SendMessage, 0x1330, 6,, SysTabControl321, FI Simulator
sleep , 100
ControlGet , Curr , Checked , , Button86 , FI Simulator
Stat = Curr
Loop , 10 {
	ControlGet , Stat , Checked , , Button86 , FI Simulator
	if (Stat <> Curr) {
		break
	}
	if (Stat == Curr) {
		ControlClick , Button86 , FI Simulator
	}
	sleep , 100
}
return

!m::
Toggle  := !Toggle
While Toggle{
	Click
	sleep , 1000
}
return

!e::
Exitapp

#p::Pause
Pause , Toggle, OperateOnUnderlyingThread
